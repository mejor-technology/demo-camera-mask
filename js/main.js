"use strict";

const dimensions = document.querySelector("#dimensions");
const video = document.querySelector("video");
const canvas = (window.canvas = document.querySelector("canvas"));
let stream;

const videoblock = document.querySelector("#videoblock");

(function () {
  $("#videoblock").width($(window).width());
  $("#videoblock").height(($(window).width() * 4) / 3);

  $("#mask").width($(window).width());
  $("#mask").height(($(window).width() * 4) / 3);

  $("#btn-open").on("click", function () {
    $("#videoblock").removeClass("hidden");
    $("#btn-take").removeClass("hidden");
    $("#btn-open").addClass("hidden");
    getMedia(fullHdConstraints);
  });
  $("#btn-take").on("click", function () {
    $("#videoblock").addClass("hidden");
    $("#btn-take").addClass("hidden");

    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas
      .getContext("2d")
      .drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
    var dataURL = canvas.toDataURL();
    $("#result").attr("src", dataURL);
    stopBothVideoAndAudio(window.stream);
    $("#btn-download").removeClass("hidden");
  });
  $("#btn-download").on("click", function () {
    html2canvas(document.querySelector("#videoblock")).then((canvas) => {
      var link = document.createElement("a");
      link.download = "filename.png";
      link.href = canvas.toDataURL();
      link.click();
    });
  });
})();

const fullHdConstraints = {
  video: { width: { exact: 1280 }, height: { exact: 720 } },
};

function gotStream(mediaStream) {
  stream = window.stream = mediaStream; // stream available to console
  video.srcObject = mediaStream;
  videoblock.style.display = "block";
  const track = mediaStream.getVideoTracks()[0];
  const constraints = track.getConstraints();
  console.log("Result constraints: " + JSON.stringify(constraints));
}

function getMedia(constraints) {
  if (stream) {
    stream.getTracks().forEach((track) => {
      track.stop();
    });
  }

  videoblock.style.display = "none";
  navigator.mediaDevices
    .getUserMedia(constraints)
    .then(gotStream)
    .catch((e) => {
      console.log(e);
    });
}

function stopBothVideoAndAudio(stream) {
  stream.getTracks().forEach(function (track) {
    if (track.readyState == "live") {
      track.stop();
    }
  });
}
